#ifndef TENSORFLOW_LITE_MICRO_HAR_INPUT_DATA_H_
#define TENSORFLOW_LITE_MICRO_HAR_INPUT_DATA_H_


extern const unsigned int input_data_len;

extern const float g_har_data[][120];

extern const unsigned int g_har_data_label[];


#endif  // TENSORFLOW_LITE_MICRO_HAR_INPUT_DATA_H_
