/* Copyright 2020 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

//#define DEBUGMESSAGES

#include <har/constants.h>
#include <har/data.h>
#include <har/har_model_data.h>
#include <har/main_functions.h>
#include <har/output_handler.h>
#include <unistd.h>

#include <math.h>

#include "tensorflow/lite/micro/all_ops_resolver.h"
#include "tensorflow/lite/micro/micro_error_reporter.h"
#include "tensorflow/lite/micro/micro_interpreter.h"
#include "tensorflow/lite/micro/system_setup.h"
#include "tensorflow/lite/schema/schema_generated.h"

// Globals, used for compatibility with Arduino-style sketches.
namespace {
tflite::ErrorReporter* error_reporter = nullptr;
const tflite::Model* model = nullptr;
tflite::MicroInterpreter* interpreter = nullptr;
TfLiteTensor* input = nullptr;
TfLiteTensor* output = nullptr;
int inference_count = 0;

constexpr int kTensorArenaSize = 10 * 1024;
uint8_t tensor_arena[kTensorArenaSize];
}  // namespace

// The name of this function is important for Arduino compatibility.
void setup() {
  tflite::InitializeTarget();

  // Set up logging. Google style is to avoid globals or statics because of
  // lifetime uncertainty, but since this has a trivial destructor it's okay.
  // NOLINTNEXTLINE(runtime-global-variables)
  static tflite::MicroErrorReporter micro_error_reporter;
  error_reporter = &micro_error_reporter;

  // Map the model into a usable data structure. This doesn't involve any
  // copying or parsing, it's a very lightweight operation.
  model = tflite::GetModel(g_har_model_data);
  if (model->version() != TFLITE_SCHEMA_VERSION) {
    TF_LITE_REPORT_ERROR(error_reporter,
                         "Model provided is schema version %d not equal "
                         "to supported version %d.",
                         model->version(), TFLITE_SCHEMA_VERSION);
    return;
  }

  // This pulls in all the operation implementations we need.
  // NOLINTNEXTLINE(runtime-global-variables)
//  static tflite::AllOpsResolver resolver;

    static tflite::MicroMutableOpResolver<5> resolver(error_reporter);
    if (resolver.AddRelu() != kTfLiteOk) {
      return;
    }
    if (resolver.AddTanh() != kTfLiteOk) {
      return;
    }
    if (resolver.AddFullyConnected() != kTfLiteOk) {
      return;
    }
    if (resolver.AddSoftmax() != kTfLiteOk) {
      return;
    }
    if (resolver.AddReshape() != kTfLiteOk) {
      return;
    }

  // Build an interpreter to run the model with.
  static tflite::MicroInterpreter static_interpreter(
      model, resolver, tensor_arena, kTensorArenaSize, error_reporter);
  interpreter = &static_interpreter;

  // Allocate memory from the tensor_arena for the model's tensors.
  TfLiteStatus allocate_status = interpreter->AllocateTensors();
  if (allocate_status != kTfLiteOk) {
    TF_LITE_REPORT_ERROR(error_reporter, "AllocateTensors() failed");
    return;
  }

  // Obtain pointers to the model's input and output tensors.
  input = interpreter->input(0);
  output = interpreter->output(0);

  // Keep track of how many inferences we have performed.
  inference_count = 0;
}

// The name of this function is important for Arduino compatibility.
void loop() {

#ifdef DEBUGMESSAGES
    TF_LITE_REPORT_ERROR(error_reporter,
                             "input Size: %d, Dim_0: %d, Dim_1: %d, Type: %d    \n",
                             input->dims->size,
                             input->dims->data[0],
                             input->dims->data[1],
                             (int) input->type);
#endif

  for (size_t i = 0; i < input_data_len; i++) {
    input->data.f[i] = g_har_data[inference_count][i];
  }

#ifdef DEBUGMESSAGES
  TF_LITE_REPORT_ERROR(error_reporter,
                           "Inputs: in[0]: %f, in[1]: %f, in[2]: %f    \n",
                           input->data.f[0],
                           input->data.f[1],
                           input->data.f[2]);
#endif


//  // Quantize the input from floating-point to integer
//  int8_t x_quantized = x / input->params.scale + input->params.zero_point;
//  // Place the quantized input in the model's input tensor
//  input->data.int8[0] = x_quantized;

  // Run inference, and report any error
  TfLiteStatus invoke_status = interpreter->Invoke();
  if (invoke_status != kTfLiteOk) {
    TF_LITE_REPORT_ERROR(error_reporter, "Invoke failed on cnt: %f\n",
                         static_cast<double>(inference_count));
    return;
  }

//  // Obtain the quantized output from model's output tensor
//  int8_t y_quantized = output->data.int8[0];
//  // Dequantize the output from integer to floating-point
//  float y = (y_quantized - output->params.zero_point) * output->params.scale;

#ifdef DEBUGMESSAGES
  TF_LITE_REPORT_ERROR(error_reporter,
                         "Output Size: %d, Dim_0: %d, Dim_1: %d, Type: %d    \n",
                         output->dims->size,
                         output->dims->data[0],
                         output->dims->data[1],
                         (int) output->type);
#endif

  int y_true = g_har_data_label[inference_count];

  // Output the results. A custom HandleOutput function can be implemented
  // for each supported hardware target.
//  HandleOutput(error_reporter, (float) inference_count, y, y_true);

  TF_LITE_REPORT_ERROR(error_reporter,
                       "x_value: %d, y_1: %d, y_2: %d, y_3: %d, y_4: %d, y_5: %d, y_6: %d, y_7: %d, y_8: %d, y_true: %d    \n",
                       inference_count,
                       (int) (output->data.f[0]*100),
                       (int) (output->data.f[1]*100),
                       (int) (output->data.f[2]*100),
                       (int) (output->data.f[3]*100),
                       (int) (output->data.f[4]*100),
                       (int) (output->data.f[5]*100),
                       (int) (output->data.f[6]*100),
                       (int) (output->data.f[7]*100),
                      y_true);

  // Increment the inference_counter, and reset it if we have reached
  // the total data points number
  inference_count++;
  if (inference_count >= 8) inference_count = 0;
}
