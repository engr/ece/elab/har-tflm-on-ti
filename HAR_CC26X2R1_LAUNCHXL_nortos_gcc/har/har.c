/*
 * Copyright (c) 2015-2019, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== hello_world.c ========
 */
#include <har/main_functions.h>
#include <stdint.h>
#include <stddef.h>
#include <unistd.h>


/* Driver Header files */
#include <ti/drivers/GPIO.h>
#include <ti/display/Display.h>
#include <ti/drivers/Power.h>
#include <ti/drivers/Watchdog.h>

/* Driver configuration */
#include "ti_drivers_config.h"

/* Tensorflow main functions header */
#include "tensorflow/lite/micro/cortex_m_generic/debug_log_callback.h"

Display_Handle displayHandle;

#define TIMEOUT_MS      1000
#define SLEEP_US        50000

void debugDisplayPrint(const char *s)
{
    static int line = 0;
    Display_printf(displayHandle, line++, 0, (char *) s);
}

/*
 *  ======== watchdogCallback ========
 */
void watchdogCallback(uintptr_t watchdogHandle)
{
    /*
     * If the Watchdog Non-Maskable Interrupt (NMI) is called,
     * loop until the device resets. Some devices will invoke
     * this callback upon watchdog expiration while others will
     * reset. See the device specific watchdog driver documentation
     * for your device.
     */
    while (1) {}
}

/*
 *  ======== mainThread ========
 */
void *mainThread(void *arg0)
{
    Watchdog_Handle watchdogHandle;
    Watchdog_Params params;
    uint32_t        reloadValue;


    /* Call driver init functions */
    GPIO_init();
    Display_init();
//    Watchdog_init();

    /* Configure & open Display driver */
    Display_Params displayParams;
    Display_Params_init(&displayParams);
    displayParams.lineClearMode = DISPLAY_CLEAR_BOTH;

    displayHandle = Display_open(Display_Type_UART, &displayParams);
    if (displayHandle == NULL) {
        while (1);
    }

    /*TFLite debug function override*/
    RegisterDebugLogCallback(debugDisplayPrint);

//    /* Open a Watchdog driver instance */
//    Watchdog_Params_init(&params);
//    params.callbackFxn = (Watchdog_Callback) watchdogCallback;
//    params.debugStallMode = Watchdog_DEBUG_STALL_ON;
//    params.resetMode = Watchdog_RESET_ON;
//
//    watchdogHandle = Watchdog_open(CONFIG_WATCHDOG_0, &params);
//    if (watchdogHandle == NULL) {
//        /* Error opening Watchdog */
//        while (1) {}
//    }
//    reloadValue = Watchdog_convertMsToTicks(watchdogHandle, TIMEOUT_MS);
//    if (reloadValue != 0) {
//        Watchdog_setReload(watchdogHandle, reloadValue);
//    }

    /* Configure the LED and button pins */
    GPIO_setConfig(CONFIG_GPIO_LED_0, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
    GPIO_setConfig(CONFIG_GPIO_LED_1, GPIO_CFG_OUT_STD | GPIO_CFG_OUT_LOW);
    GPIO_setConfig(CONFIG_GPIO_BUTTON_0, GPIO_CFG_IN_PU | GPIO_CFG_IN_INT_FALLING);

    /* Turn on user LED */
    GPIO_write(CONFIG_GPIO_LED_0, CONFIG_GPIO_LED_ON);
    GPIO_write(CONFIG_GPIO_LED_1, CONFIG_GPIO_LED_ON);

    Display_printf(displayHandle, 0, 0, "Hello! Initializing TFLite for Micro...\r\n");
    setup();
    Display_printf(displayHandle, 1, 0, "Init Done. Running...\r\n");
    /* Toggle an LED */
    GPIO_toggle(CONFIG_GPIO_LED_0);
    GPIO_toggle(CONFIG_GPIO_LED_1);
    int cnt = 0;
    while (1) {
        /*
         * Enabling power policy will allow the device to enter a low
         * power state when the CPU is idle. How the watchdog peripheral
         * behaves in a low power state is device specific.
         */
        Power_enablePolicy();

        /* Sleep for SLEEP_US before clearing the watchdog */
        sleep(2);
//        Watchdog_clear(watchdogHandle);
//        GPIO_toggle(CONFIG_GPIO_LED_0);

        Power_disablePolicy();
        loop();
//        Display_printf(displayHandle, cnt+2, 0, "cnt: %d - EA: %d\n", cnt, EA);
        cnt += 1;
    }
}
