/* Copyright 2019 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

#include <har/output_handler.h>

void HandleOutput(tflite::ErrorReporter* error_reporter, float x_value,
                  float y_pred[], float y_true) {
  // Log the current X and Y values
//  TF_LITE_REPORT_ERROR(error_reporter, "x_value: %f, y_1: %f, y_2: %f, y_3: %f, y_4: %f, y_5: %f, y_6: %f, y_7: %f, y_8: %f, y_true: %f\n",
//                       static_cast<double>(x_value),
//                       static_cast<double>(y_pred[0]),
//                       static_cast<double>(y_pred[1]),
//                       static_cast<double>(y_pred[2]),
//                       static_cast<double>(y_pred[3]),
//                       static_cast<double>(y_pred[4]),
//                       static_cast<double>(y_pred[5]),
//                       static_cast<double>(y_pred[6]),
//                       static_cast<double>(y_pred[7]),
//                       static_cast<double>(y_true));

    TF_LITE_REPORT_ERROR(error_reporter, "x_value: %f, y_1: %f, y_2: %f, y_3: %f, y_4: %f, y_5: %f, y_6: %f, y_7: %f, y_8: %f, y_true: %f\n",
                           x_value,
                           y_pred[0],
                           y_pred[1],
                           y_pred[2],
                           y_pred[3],
                           y_pred[4],
                           y_pred[5],
                           y_pred[6],
                           y_pred[7],
                           y_true);
}
