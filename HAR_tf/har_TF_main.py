# -*- coding: utf-8 -*-
"""
Created on Tu Feb 1 2022

@author: Yigit Tuncel, Sizhe An, Ganapati Bhat
"""
import tensorflow as tf
from tensorflow.keras.layers import Dense, Dropout, Flatten, Activation, Conv2D, MaxPooling2D
from tensorflow.keras import Sequential
from tensorflow.keras import backend as K
import pandas as pd
import numpy as np
import random as rn
import os
import time
import csv
from datetime import datetime
# Set TF to run on GPU - Our system has two GPUs, run on the second one.
os.environ["CUDA_VISIBLE_DEVICES"]="1" # second gpu
physical_devices = tf.config.list_physical_devices('GPU'); tf.config.experimental.set_memory_growth(physical_devices[0], True)

os.environ['PYTHONHASHSEED'] = '0'
np.random.seed(56)
rn.seed(1245)
startTime = datetime.now() # Get the start time of the code
pd.options.mode.chained_assignment = None  # default='warn'


# Callback for printing the accuracy difference
class Acc_diff_history(tf.keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.prev_acc = 0
        self.curr_acc = 0
        self.i = 1

    def on_epoch_end(self, batch, logs={}):
        self.curr_acc = logs.get('val_categorical_accuracy')
        acc_diff = self.curr_acc - self.prev_acc
        self.prev_acc = self.curr_acc
        print("H1: ", curr_h1, "H2: ", curr_h2, "H3: ", curr_h3, "Fold: ", curr_fold, " Epoch: ", self.i, "Accuracy difference is ", 
              (acc_diff), "and current val. accuracy is ", self.curr_acc)
        self.i = self.i + 1

# outputs
'''
[0.66047596406559383, 0.3547245744908703, ..., 0.25953155204159617, 0.25901699725311789]
'''

# function to reset the keras python seeds
def reset_random_seeds():
    os.environ['PYTHONHASHSEED'] = '0'

# Function to create a model
def create_model(data_train, lambda_in, num_hidden_layers, num_hidden_layer_neurons):
    n_feat = np.size(data_train, 1) # Number of features
    

    initial_learning_rate = 0.001
                                                           # Should be between 0 and 1. It helps reduce overfitting by removing % of connections in the NN

    
    #data_length
    # create model
    model = Sequential()
    
    # Add the First layer manually. This is because we want the number of neurons
    # be equal to the number of neurons in the first hidden layer and the numper of 
    # inputs to be equal to the number of features
    
    model.add(Dense(num_hidden_layer_neurons[0], input_dim=n_feat, kernel_initializer='random_uniform', activation='relu',
                      kernel_regularizer=tf.keras.regularizers.l1(lambda_in),
                      activity_regularizer=tf.keras.regularizers.l1(lambda_in)))


    # Hidden layers 2 to num_hidden_layers
    for h in range(1,num_hidden_layers):
        model.add(Dense(num_hidden_layer_neurons[h], kernel_initializer='random_uniform', activation='relu'))


    # Output Layer. Add regularization
    model.add(Dense(num_actions, kernel_initializer='random_uniform', activation='softmax'))
    

    # Compile the model. Loss function is categorical crossentropy
    model.compile(loss=tf.keras.losses.CategoricalCrossentropy(), 
                  optimizer=tf.keras.optimizers.Adam(learning_rate=initial_learning_rate), 
                  metrics=['categorical_accuracy'])
    
    model.summary()
    
    return model
    
# Function for trainig a neural network
def train_and_evaluate_model(model, data_train, label_train,
                              data_test, label_test, data_xval, label_xval):
    
    num_epochs = 240 #2000
    custom_batch_size = 50
    
    # Fit the model
    history = model.fit(data_train, label_train, epochs=num_epochs, \
                        validation_data=(data_xval, label_xval), \
                        batch_size=custom_batch_size, callbacks=callbacks_list, shuffle=True, verbose=0, class_weight = har_class_weights)
    
    # evaluate the model
    scores = model.evaluate(data_test, label_test)
    #print("\n%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))
    #if (np.array_equal(labels[test], labels[train])): # we do CV
    model_accuracy_test = scores[1]*100
    
    # evaluate the model for training data
    scores = model.evaluate(data_train, label_train)
    model_accuracy_train = scores[1]*100
    
        # evaluate the model for xval data
    scores = model.evaluate(data_xval, label_xval)
    model_accuracy_xval = scores[1]*100
    
    print(model_accuracy_train, model_accuracy_xval, model_accuracy_test)
    #else:
    #    model_accuracy = np.multiply(history.history['acc'], 100)
    # Get the predicted data [Just for debugging]
    #predicted_action = model.predict(train_data, verbose=1)
    #predicted_action_class = model.predict_classes(train_data, verbose=1)
    return history, model_accuracy_train,model_accuracy_test, model_accuracy_xval, model


# learning rate schedule
def step_decay(epoch):
    lrate = K.get_value(model.optimizer.lr)
    #print("learning rate is", lrate)  
    if (epoch > 0):
        curr_test_accuracy = (hist.history.get('val_categorical_accuracy')[-1])
        if(curr_test_accuracy > 0.94):
            lrate = lrate*lr_decay_rate
        elif (curr_test_accuracy > 0.90):
            lrate = 0.001/2
    #if (score > 10500):
    #    lrate = 0.0#0.001
    
    return lrate

"""
MAIN CODE STARTS HERE
"""

n_folds = 5 # K-fold crossvalidation

# learning rate decay rate
lr_decay_rate = 0.95

histories = []
# define class weights
#% Jump = 1
#% Lie down = 2
#% Sit = 3
#% Stand = 4
#% Walk = 5
#% Stairsup = 6
#% Stairs down = 7
#% Transition  = 8
har_class_weights = {0:4,
    1:1.2,
    2:1.5,
    3:1.7,
    4:2,
    5:4,
    6:4.5,
    7:6}



output_folder = "Output"
if not os.path.exists(output_folder):
    os.mkdir(output_folder)
    
df_features_train = pd.read_csv('HAR_v2/' + 'features' + '/train_data' +'.csv', header=None)
df_labels_train = pd.read_csv('HAR_v2/' + 'features' + '/train_labels' +'.csv', header=None)
		
df_features_test = pd.read_csv('HAR_v2/' + 'features' + '/test_data' +'.csv', header=None)
df_labels_test = pd.read_csv('HAR_v2/' + 'features' + '/test_labels' +'.csv', header=None)   
		
df_features_xval = pd.read_csv('HAR_v2/' + 'features' + '/xval_data' +'.csv', header=None)
df_labels_xval = pd.read_csv('HAR_v2/' + 'features' + '/xval_labels' +'.csv', header=None)  


n = np.size(df_features_train, 1) # Number of columns



'''
ANN for predicting the optimal action given a set of feature data
I suspect we will have to do kmeans clustering here. 
We should be careful about how different each of the feature inputs are.
'''
'''
Perform resampling in such as way that there is equal representation 
    of all the classes
    '''
# Get data for training and test
data_train = df_features_train
labels_train = df_labels_train.to_numpy()
labels_train = labels_train - 1
labels_train = labels_train.ravel()
labels_orig_train = labels_train

# Extract test data
data_test = df_features_test
labels_test = df_labels_test.to_numpy()
labels_test = labels_test - 1
labels_test = labels_test.ravel()
labels_orig_test = labels_test

# Extract xval data
data_xval = df_features_xval
labels_xval = df_labels_xval.to_numpy()
labels_xval = labels_xval - 1
labels_xval = labels_xval.ravel()
labels_orig_xval = labels_xval

num_actions = 8

# Convert labels
labels_train = tf.keras.utils.to_categorical(labels_train, num_actions)
labels_test = tf.keras.utils.to_categorical(labels_test, num_actions)
labels_xval = tf.keras.utils.to_categorical(labels_xval, num_actions)

'''
Perform n-fold cross validation
'''
model_accuracy_train = np.zeros((n_folds, 1))
model_accuracy_test = np.zeros((n_folds, 1))
model_accuracy_xval = np.zeros((n_folds, 1))
model_accuracy_overall = np.zeros((n_folds, 1))


# difference in acc
acc_diff_track = Acc_diff_history()
# Monitor the validation accuracy
# min_delta: $min_delta change in the monitor quantity (val_acc) is considered an improvement
# patience: $patience epochs with no improvement can be tolerated 
earlystop = tf.keras.callbacks.EarlyStopping(monitor='val_categorical_accuracy', min_delta=0.1, patience=5, \
                      verbose=1, mode='auto')

reduce_lr = tf.keras.callbacks.ReduceLROnPlateau(monitor='val_categorical_accuracy', factor=0.95, \
                          patience=20, min_lr=0.001)


hist = tf.keras.callbacks.History()
# learning schedule callback
lrate = tf.keras.callbacks.LearningRateScheduler(step_decay)

callbacks_list = [hist, lrate]
    
csv_header = ['h1 neurons', 'h2 neurons', 'h3 neurons','Iter','Train','xval', 'Test']       # write header
flog = open(output_folder + '/accuracy_log' +'.csv', "w", newline='')
writer = csv.writer(flog)
writer.writerow(csv_header)         

train_indices = np.array([])
test_indices = np.array([])
curr_fold = 0
curr_h1 = 0
curr_h2 = 0
curr_h3 = 0
curr_lambda = 0.00009

hidden_layer_1_neurons = [4]
num_h1_neurons = len(hidden_layer_1_neurons)                                    # Length of the array

hidden_layer_2_neurons = [8]
num_h2_neurons = len(hidden_layer_2_neurons)                                    # Length of the array

hidden_layer_3_neurons = [8]
num_h3_neurons = len(hidden_layer_3_neurons)                                    # Length of the array

num_hidden_layers_total = 2

# Sweep through the number of layers in the hidden layers
# Number of neurons in H1, H2 and H3
for ih1 in range(0, num_h1_neurons):
    for ih2 in range(0, num_h2_neurons):
        for ih3 in range(0, num_h3_neurons):

            # Extract the number of neurons in each layer
            curr_h1 = hidden_layer_1_neurons[ih1]
            curr_h2 = hidden_layer_2_neurons[ih2]
            curr_h3 = hidden_layer_3_neurons[ih3]
        
        # Create an array with hidden layer neurons
            hidden_layer_neuron = [curr_h1, curr_h2, curr_h3]
        
        # Now run the training for n_folds
            for i in range(0,n_folds):
                print("Running Fold", i+1, "/", n_folds)
                
                curr_fold = i
                filename_log = output_folder + '/training_h1_{0}_h2_{1}_h3_{2}_iter_{3}.log'.format(curr_h1, curr_h2,
                                                        curr_h3, i)
                if (i != 0):
                    del callbacks_list[-1]
                csv_logger = tf.keras.callbacks.CSVLogger(filename_log)
                callbacks_list.append(csv_logger)
                model = None # Clearing the NN.
                model = create_model(data_train.loc[:,:], curr_lambda, num_hidden_layers_total, hidden_layer_neuron)
                history, model_accuracy_temp, model_test, model_xval, model = \
                        train_and_evaluate_model(model,data_train.loc[:,:],labels_train[:], \
                                                     data_test.loc[:,:], labels_test[:], \
                                                     data_xval.loc[:,:], labels_xval[:])
                histories.append(history)
                model_accuracy_train[i] = model_accuracy_temp
                model_accuracy_test[i] = model_test
                curr_fold_accuracy = (curr_h1, curr_h2, curr_h3, i+1, model_accuracy_temp, model_xval, model_test)
                writer.writerow(curr_fold_accuracy) 
                filename = output_folder + '/model_final_scaled_h1_{0}_h2_{1}_h3_{2}_iter_{3}'.format(curr_h1, curr_h2,
                                                              curr_h3, i)
                filename_weights = output_folder + '/model_final_scaled_h1_{0}_h2_{1}_h3_{2}_iter{3}.csv'.format(curr_h1, curr_h2,
                                                                      curr_h3, i)
                # Save the model
                model.save(filename)
                f=open(filename_weights,'w',newline='')
            
            # Save the weight to CSV
                for layer in model.layers:
                    curr_weights = layer.get_weights()
                    for jj in range(len(curr_weights)):
                        #writer.writerows(map(lambda x: [x], curr_weights[jj]))
                        np.savetxt(f, curr_weights[jj], fmt='%0.16f', delimiter=',', newline='\n')
                        f.write("\n\n")
                f.close()
        
        # Remove the csv logger
            del callbacks_list[-1]

 
flog.close()     
print('Average Accuracy: ', np.mean(model_accuracy_test), '%')


print('The entire code running time is ', datetime.now() - startTime)


# Convert the model to tflite.
converter = tf.lite.TFLiteConverter.from_keras_model(model)
tflite_model = converter.convert()

# Save the tflite model.
with open('model.tflite', 'wb') as f:
  f.write(tflite_model)


# Use the following code in linux to convert the .tflite model to .cc file

# # Install xxd if it is not available
# !apt-get update && apt-get -qq install xxd
# # Convert to a C source file, i.e, a TensorFlow Lite for Microcontrollers model
# !xxd -i {MODEL_TFLITE} > {MODEL_TFLITE_MICRO}
# # Update variable names
# REPLACE_TEXT = MODEL_TFLITE.replace('/', '_').replace('.', '_')
# !sed -i 's/'{REPLACE_TEXT}'/g_model/g' {MODEL_TFLITE_MICRO}
