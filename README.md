# HAR-TFLM-on-TI

Our lab's Human Activity Recognition (HAR) Application implemented on TI CC2652R SoC using Tensorflow Lite for Micro.

## Python Model Training and Conversion to TFLM [-TAKE ME TO CODE-](HAR_tf/har_TF_main.py)

For training, we will use a dataset collected in our lab. 
The dataset consists of sensor data of 22 users for 8 different activities. 
The details about the dataset, experimental setup and procedure can be found in the following references: 

- `Bhat, Ganapati, et al. "w-HAR: An activity recognition dataset and framework using low-power wearable devices." Sensors 20.18 (2020): 5356.`
- `Bhat, Ganapati, et al. "Online human activity recognition using low-power wearable devices." 2018 IEEE/ACM International Conference on Computer-Aided Design (ICCAD). IEEE, 2018.`

We provide the dataset and a sample script that trains a neural network in [HAR_tf](HAR_tf).
The csv files in the `features` subdirectory contains extracted features from the collected sensor data. 
Each observation has `120` features and a corresponding `activity label`. 
In this first part of the application, we are going to train a `Tensorflow` neural network model on this data. 
We use the `Keras` frontend to implement the model.

In this implementation, we chose to use a 3-layer neural network with 4-8-8 neurons in each layer, respectively. 
We use `Dense` layers and no `dropout` layers in between. 
We use `RELU` as the activation function, and `softmax` as the output function.

### Prerequisites

We use anaconda to manage the environment. 
Create a new environment using `spec-file.txt` with the following command:

```bash
conda create --name <your-env-name> --file spec-file.txt
```
After this step, we install `pandas` and `tensorflow` using the `pip` package manager:

```bash
pip install pandas
pip install tensorflow
```

NOTE: At the time of writing this post, there is a problem with anaconda’s package manager. 
When these two packages are included in spec-file.txt, an old version of the numpy package is installed. 
This causes a bunch of other packages, including tensorflow, to complain and sometimes throw errors. 
For this reason we install them separately. 
However, this might be fixed along the way, or may even be an isolated incident only on our end, so feel free to include these also in the spec-file.txt and give it a shot for a better flow.

Once the environment is created and all the packages are installed, activate it with:

```bash
conda activate <your-env-name>
```

### Running the training code

The training script is in `HAR_tf/har_TF_main.py`.
The below lines control which GPU Tensorflow should run on. 
Feel free to comment out these lines if your system has a single GPU.

```python
# Set TF to run on GPU - Our system has two GPUs, run on the second one.
os.environ["CUDA_VISIBLE_DEVICES"]="1" # second gpu
physical_devices = tf.config.list_physical_devices('GPU') 
tf.config.experimental.set_memory_growth(physical_devices[0], True)
```

Make sure you activate the environment `<your-env-name>`.
Then, you can run the script directly from the terminal:

```bash
(har-env) tuncel@elab:~/HAR_tensorflow$ python har_TF_main_3.py
```

The code takes around `an hour` to run on a RTX3090 GPU, and you should see the below output:

<p>
<details>
<summary><strong>Click to collapse/fold.</strong></summary>

<pre><code>Running Fold 1 / 5
2022-02-14 15:46:30.543686: I tensorflow/core/platform/cpu_feature_guard.cc:151] This TensorFlow binary is optimized with oneAPI Deep Neural Network Library (oneDNN) to use the following CPU instructions in performance-critical operations:  AVX2 AVX512F FMA
To enable them in other operations, rebuild TensorFlow with the appropriate compiler flags.
2022-02-14 15:46:31.068753: I tensorflow/core/common_runtime/gpu/gpu_device.cc:1525] Created device /job:localhost/replica:0/task:0/device:GPU:0 with 22104 MB memory:  -> device: 0, name: NVIDIA GeForce RTX 3090, pci bus id: 0000:af:00.0, compute capability: 8.6
Model: "sequential"
_________________________________________________________________
 Layer (type)                Output Shape              Param #   
=================================================================
 dense (Dense)               (None, 4)                 484       
                                                                 
 dense_1 (Dense)             (None, 8)                 40        
                                                                 
 dense_2 (Dense)             (None, 8)                 72        
                                                                 
=================================================================
Total params: 596
Trainable params: 596
Non-trainable params: 0
_________________________________________________________________
2022-02-14 15:46:32.991794: I tensorflow/stream_executor/cuda/cuda_blas.cc:1786] TensorFloat-32 will be used for the matrix multiplication. This will only be logged once.
38/38 [==============================] - 0s 2ms/step - loss: 0.4252 - categorical_accuracy: 0.9157
109/109 [==============================] - 0s 3ms/step - loss: 0.1581 - categorical_accuracy: 0.9634
36/36 [==============================] - 0s 2ms/step - loss: 0.3369 - categorical_accuracy: 0.9180
96.3379442691803 91.79755449295044 91.56829714775085
2022-02-14 15:48:00.695039: W tensorflow/python/util/util.cc:368] Sets are not currently considered sequences, but this may change in the future, so consider avoiding using them.
Running Fold 2 / 5
Model: "sequential_1"
_________________________________________________________________
 Layer (type)                Output Shape              Param #   
=================================================================
 dense_3 (Dense)             (None, 4)                 484       
                                                                 
 dense_4 (Dense)             (None, 8)                 40        
                                                                 
 dense_5 (Dense)             (None, 8)                 72        
                                                                 
=================================================================
Total params: 596
Trainable params: 596
Non-trainable params: 0
_________________________________________________________________
38/38 [==============================] - 0s 2ms/step - loss: 0.3917 - categorical_accuracy: 0.9258
109/109 [==============================] - 0s 3ms/step - loss: 0.1490 - categorical_accuracy: 0.9622
36/36 [==============================] - 0s 2ms/step - loss: 0.3907 - categorical_accuracy: 0.9110
96.22260928153992 91.09947681427002 92.58010387420654
Running Fold 3 / 5
Model: "sequential_2"
_________________________________________________________________
 Layer (type)                Output Shape              Param #   
=================================================================
 dense_6 (Dense)             (None, 4)                 484       
                                                                 
 dense_7 (Dense)             (None, 8)                 40        
                                                                 
 dense_8 (Dense)             (None, 8)                 72        
                                                                 
=================================================================
Total params: 596
Trainable params: 596
Non-trainable params: 0
_________________________________________________________________
38/38 [==============================] - 0s 2ms/step - loss: 0.3630 - categorical_accuracy: 0.9148
109/109 [==============================] - 0s 3ms/step - loss: 0.1693 - categorical_accuracy: 0.9640
36/36 [==============================] - 0s 3ms/step - loss: 0.3737 - categorical_accuracy: 0.9171
96.39561772346497 91.71029925346375 91.48398041725159
Running Fold 4 / 5
Model: "sequential_3"
_________________________________________________________________
 Layer (type)                Output Shape              Param #   
=================================================================
 dense_9 (Dense)             (None, 4)                 484       
                                                                 
 dense_10 (Dense)            (None, 8)                 40        
                                                                 
 dense_11 (Dense)            (None, 8)                 72        
                                                                 
=================================================================
Total params: 596
Trainable params: 596
Non-trainable params: 0
_________________________________________________________________
38/38 [==============================] - 0s 2ms/step - loss: 0.3421 - categorical_accuracy: 0.9300
109/109 [==============================] - 0s 3ms/step - loss: 0.1166 - categorical_accuracy: 0.9720
36/36 [==============================] - 0s 2ms/step - loss: 0.3280 - categorical_accuracy: 0.9267
97.2029983997345 92.6701545715332 93.00168752670288
Running Fold 5 / 5
Model: "sequential_4"
_________________________________________________________________
 Layer (type)                Output Shape              Param #   
=================================================================
 dense_12 (Dense)            (None, 4)                 484       
                                                                 
 dense_13 (Dense)            (None, 8)                 40        
                                                                 
 dense_14 (Dense)            (None, 8)                 72        
                                                                 
=================================================================
Total params: 596
Trainable params: 596
Non-trainable params: 0
_________________________________________________________________
38/38 [==============================] - 0s 3ms/step - loss: 0.3136 - categorical_accuracy: 0.9216
109/109 [==============================] - 0s 3ms/step - loss: 0.1576 - categorical_accuracy: 0.9611
36/36 [==============================] - 0s 2ms/step - loss: 0.3026 - categorical_accuracy: 0.9232
96.10726833343506 92.321115732193 92.15851426124573
Average Accuracy:  92.15851664543152 %
The entire code running time is  0:50:26.786062
2022-02-14 15:53:40.148655: W tensorflow/compiler/mlir/lite/python/tf_tfl_flatbuffer_helpers.cc:357] Ignored output_format.
2022-02-14 15:53:40.148694: W tensorflow/compiler/mlir/lite/python/tf_tfl_flatbuffer_helpers.cc:360] Ignored drop_control_dependency.
2022-02-14 15:53:40.149457: I tensorflow/cc/saved_model/reader.cc:43] Reading SavedModel from: /tmp/tmp1p9zzwlh
2022-02-14 15:53:40.150878: I tensorflow/cc/saved_model/reader.cc:78] Reading meta graph with tags { serve }
2022-02-14 15:53:40.150900: I tensorflow/cc/saved_model/reader.cc:119] Reading SavedModel debug info (if present) from: /tmp/tmp1p9zzwlh
2022-02-14 15:53:40.158506: I tensorflow/cc/saved_model/loader.cc:228] Restoring SavedModel bundle.
2022-02-14 15:53:40.196588: I tensorflow/cc/saved_model/loader.cc:212] Running initialization op on SavedModel bundle at path: /tmp/tmp1p9zzwlh
2022-02-14 15:53:40.209690: I tensorflow/cc/saved_model/loader.cc:301] SavedModel load for tags { serve }; Status: success: OK. Took 60235 microseconds.
</code></pre>

</details>
</p>

The script saves the tflite model as `model.tflite`. 
To convert it to a C byte array, use the following commands to install `xxd` and do a hexdump using it:

```bash
apt-get update && apt-get -qq install xxd
xxd -i model.tflite > model_TFLM.h
```

At this point, our trained model is converted to a C byte array, which is available in `model_TFLM.h`.
As the next step, we will deploy this model on the TI CC26X2 launchpad using the `Tensorflow Lite for Micro` library.

## Deploy Model on TI CC26X2 Launchpad [-TAKE ME TO CODE-](HAR_CC26X2R1_LAUNCHXL_nortos_gcc/har/har.c)

We use TI CC26X2R1 LaunchPad development kit for this application.
We provide a sample TI CCS project in [HAR_CC26X2R1_LAUNCHXL_nortos_gcc](HAR_CC26X2R1_LAUNCHXL_nortos_gcc), which contains all the necessary build settings.

### Software Requirements
In order to successfully build the sample project, please:

- Download and install Code Composer Studio 11 (CCS)
   - When prompted select "SimpleLink CC13xx and CC26xx Wireless MCUs" component 
- Download and install the SimpleLink CC13xx CC26xx SDK 
   - We used v5.30.1.01
- Install the ARM GCC toolchain
   - We used v9.2.1

After these steps, you should be able to 
- import the project into CCS, 
- build it, and 
- flash/debug it on the launchpad. 

The trained model is located in `har_model_data.cc`. 
If you make any changes to the python model and re-train, you should replace the bytearray in this file with the newly generated model.
The main functionality is implemented in `main_functions.cc`.
In setup(), we instantiate the model and the pointers to its input and output.
In loop(), we load a feature set into the memory pointed by `*input`, invoke the inference of the model, and then print the output of the model.

To observe the output, open a serial terminal (e.g. putty, mobaxterm) on the COM Port that corresponds to XDS110 Class Application/User UART (it’s COM5 on our setup). 
You should see the following output on the terminal:

```bash
x_value: 0, y_1: 99, y_2: 0, y_3: 0, y_4: 0, y_5: 0, y_6: 0, y_7: 0, y_8: 0, y_true: 1
x_value: 1, y_1: 0, y_2: 95, y_3: 1, y_4: 0, y_5: 0, y_6: 0, y_7: 0, y_8: 1, y_true: 2
x_value: 2, y_1: 0, y_2: 0, y_3: 79, y_4: 0, y_5: 0, y_6: 0, y_7: 4, y_8: 16, y_true: 3
x_value: 3, y_1: 0, y_2: 0, y_3: 7, y_4: 74, y_5: 0, y_6: 0, y_7: 0, y_8: 17, y_true: 4
x_value: 4, y_1: 0, y_2: 0, y_3: 0, y_4: 0, y_5: 99, y_6: 0, y_7: 0, y_8: 0, y_true: 5
x_value: 5, y_1: 0, y_2: 0, y_3: 0, y_4: 0, y_5: 0, y_6: 99, y_7: 0, y_8: 0, y_true: 6
x_value: 6, y_1: 0, y_2: 0, y_3: 0, y_4: 0, y_5: 0, y_6: 0, y_7: 99, y_8: 0, y_true: 7
x_value: 7, y_1: 0, y_2: 0, y_3: 0, y_4: 0, y_5: 6, y_6: 0, y_7: 0, y_8: 92, y_true: 8
```

In this case, we chose random entries for each class from `test_data.csv` and fed them to model. 
You can change the input data in the file `data.cc`.
As you can see, the highest probability in each case match the expected *y_true* label.
